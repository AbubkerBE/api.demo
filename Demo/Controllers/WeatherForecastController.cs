﻿using Microsoft.AspNetCore.Mvc;

namespace Demo.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class WeatherForecastController(ILogger<WeatherForecastController> logger) : ControllerBase
{
    private static readonly string[] Summaries = {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly ILogger<WeatherForecastController> _logger = logger;

    [HttpGet(Name = "GetWeatherForecast")]
    public IEnumerable<WeatherForecast> Get()
    {
        return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            (
                Date : DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                TemperatureC : Random.Shared.Next(-20, 55),
                Summary : Summaries[Random.Shared.Next(Summaries.Length)]
            ))
            .ToArray();
    }
    
    
    [HttpPost(Name = "PostData")]
    public bool PostData( [FromBody] PostDataRequest request)   
    {
        if (request.Date >= DateTime.Now && request.TemperatureC is > -20 and < 55 && request.Summary != null)
            return true;
        return false;
    }
}

public class PostDataRequest
{
    public DateTime Date { get; set; }
    public int TemperatureC { get; set; }
    public string Summary { get; set; }
}       
using System.Net;
using AutoFixture.Xunit2;
using Demo.Controllers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;

namespace TestDemo;

public abstract class Tests
{
    public class WeatherForecastControllerTests : TestBase
    {
        
        [Fact]
        public async Task Get_ReturnsWeatherForecasts()
        {
            var result = await AppHttpClient.GetAsync("/WeatherForecast/Get");
            result.Content.Should().NotBeNull();
            result.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        
        [Theory]
        [InlineAutoData()]
        public async Task Post_Data(DateTime date, int temperatureC, string summary)
        {
            var request = new PostDataRequest
            {
                Date = date,
                TemperatureC = temperatureC,
                Summary = summary
            };

            var jsonRequest = JsonConvert.SerializeObject(request);
            var httpContent = new StringContent(jsonRequest);
            httpContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            
            var result = await AppHttpClient.PostAsync("/WeatherForecast/PostData", httpContent);
            //result.Content.Should().Be(true);
            result.StatusCode.Should().Be(HttpStatusCode.OK);
        }
    }
}

public class TestBase
{
    private TestWebApplicationFactory<Program> Factory;
    protected static HttpClient AppHttpClient = null!;

    protected TestBase()
    {
        Factory = new TestWebApplicationFactory<Program>();
        AppHttpClient = Factory.CreateClient();
    }
}

public class TestWebApplicationFactory<TProgram> : WebApplicationFactory<TProgram> where TProgram : Program
{
}